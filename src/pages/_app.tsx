import '@/styles/globals.css'
import './GyroCube.css';
import './Toggle.css';
import type { AppProps } from 'next/app'

export default function App({ Component, pageProps }: AppProps) {
  return <Component {...pageProps} />
}
