import dynamic from 'next/dynamic'

const GyroCube = dynamic(
  () => import("../components/GyroCube").then((mod) => mod.GyroCube),
  {
    ssr: false,
  }
);

export default function Home() {
  return (
    <>
      <GyroCube/>
    </>
  )
}
